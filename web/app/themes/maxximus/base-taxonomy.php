<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?> style="background-image: url(<?php esc_url(the_field('bgr_add', 'option')); ?>);">
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->


    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <div class="site siteSeries">

    <div class="wrapProduct" role="document">
          <div class="seriesContent">
              <?php include Wrapper\template_path(); ?>
          </div><!-- /.content -->
    </div><!-- /.wrap -->
    </div>
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>

  </body>
</html>
