<?php

function product_init() {
	register_post_type( 'product', array(
		'labels'            => array(
			'name'                => __( 'Products', 'max' ),
			'singular_name'       => __( 'Product', 'max' ),
			'all_items'           => __( 'All Products', 'max' ),
			'new_item'            => __( 'New product', 'max' ),
			'add_new'             => __( 'Add New', 'max' ),
			'add_new_item'        => __( 'Add New product', 'max' ),
			'edit_item'           => __( 'Edit product', 'max' ),
			'view_item'           => __( 'View product', 'max' ),
			'search_items'        => __( 'Search products', 'max' ),
			'not_found'           => __( 'No products found', 'max' ),
			'not_found_in_trash'  => __( 'No products found in trash', 'max' ),
			'parent_item_colon'   => __( 'Parent product', 'max' ),
			'menu_name'           => __( 'Products', 'max' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'       => true,
		'rewrite'           => array("slug" => "produkt"),
		'query_var'         => true,
		'menu_icon'         => 'dashicons-cart',
		'show_in_rest'      => true,
		'rest_base'         => 'product',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'product_init' );

function product_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['product'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Product updated. <a target="_blank" href="%s">View product</a>', 'max'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'max'),
		3 => __('Custom field deleted.', 'max'),
		4 => __('Product updated.', 'max'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Product restored to revision from %s', 'max'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Product published. <a href="%s">View product</a>', 'max'), esc_url( $permalink ) ),
		7 => __('Product saved.', 'max'),
		8 => sprintf( __('Product submitted. <a target="_blank" href="%s">Preview product</a>', 'max'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Product scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview product</a>', 'max'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Product draft updated. <a target="_blank" href="%s">Preview product</a>', 'max'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'product_updated_messages' );
