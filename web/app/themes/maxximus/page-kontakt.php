
    <?php while (have_posts()) : the_post(); ?>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pageKontakt">
            <?php echo do_shortcode('[contact-form-7 id="86" title="Kontakt"]'); ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pageKontakt">
            <article>
                <?php get_template_part('templates/page', 'header'); ?>
                <?php get_template_part('templates/content', 'page'); ?>

                <form class="newsletter" method="post" action="https://app.freshmail.com/en/actions/subscribe/"><br />
                    <input type="hidden" name="subscribers_list_hash" value="tcgxhjvd5t" />
                    <input type="text" id="freshmail_email" name="freshmail_email" placeholder="Podaj swój adres e-mail"/>
                    <input type="submit" value="Zapisuję się do newslettera*" />
                </form>
            </article>

        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pageKontakt">
            <footer>
                <?php the_field('kontakt_info'); ?>
            </footer>
        </div>
    <?php endwhile; ?>
