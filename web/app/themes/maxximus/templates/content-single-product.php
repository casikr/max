<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
      <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <figure>
                  <a data-lightbox="galeria" href="<?php the_post_thumbnail_url(); ?>">
                  <?php the_post_thumbnail('full', array("class" => 'img-fluid full-img')); ?>

                </a>
              </figure>


          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padRight">
              <header>
                <?php $terms = wp_get_post_terms($post->ID, 'series');
                if($terms) {
                    echo '<ul class="terms">';
                    foreach ($terms as $term) {
                        # code...
                        echo '<li>' . $term->name . '<span>,</span>' . "&nbsp;" . '</li>';
                    }
                    echo '</ul>';

                }

                ?>
                <hr>
                <h1 class="entry-title"><?php the_title(); ?></h1>

                <?php if(get_field( 'short_desc' )) { ?>
                    <h3><?php the_field('short_desc'); ?></h3>
                <?php } ?>
                <hr>
              </header>
              <div class="entry-content">
                <?php the_content(); ?>
              </div>
              <footer>
                <a href="<?php esc_url(the_field('shop_link')); ?>">Oficjalny sklep</a>
              </footer>
          </div>

            <div class="col-lg-6 col-md-6
            col-sm-12 col-xs-12">


            <?php $galeria = get_field('galeria');

                if($galeria) { ?>
                    <ul id="gallery">
                        <?php
                            $out = '';
                            foreach ($galeria as $img)
                            {
                                # code...
                                $out .='<li>';
                                  $out .= '<a data-lightbox="galeria" href="'. $img['url']. '">';
                                      $out .= '<img class="img-fluid" src="' . $img['sizes']['gallery']. '" alt="' . $img['alt']. '" title="' . $img['title']. '" />';
                                  $out .= '</a>';
                                $out .= '</li>';
                            }
                            echo $out;
                            ?>
                     </ul>
                 <?php } ?>
                 </div>
          
      </div>
  </article>
<?php endwhile; ?>
