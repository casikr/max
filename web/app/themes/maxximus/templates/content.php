<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 singleBlog">
    <article <?php post_class(); ?>>
      <figure>
          <?php the_post_thumbnail("gallery"); ?>
      </figure>
      <div class="entry-summary">
          <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
          <hr>
          <?php the_excerpt(); ?>
          <a class="singleLink" href="<?php esc_url(the_permalink()); ?>"><?php _e("read more >", "max"); ?></a>
      </div>
    </article>
</div>
