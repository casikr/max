<article class="page-about">
    <?php get_template_part('templates/page', 'header'); ?>
    <?php the_content(); ?>
</article>
