<?php
use Roots\Sage\Assets;
?>
<header id="mainHeader" class="banner">
  <!-- <div class="container"> -->
    <!-- <a class="brand" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a> -->
    <img id="max" src="<?php echo Assets\asset_path('images/max.png'); ?>" alt="">
    <nav id="langs">
        <?php icl_post_languages(); ?>
    </nav>
    <nav class="nav-primary">
    <img id="ham" src="<?php echo Assets\asset_path('images/arrows_hamburger.svg'); ?>" alt="">
      <?php
      if('product' == get_post_type()) {
        wp_nav_menu(['theme_location' => 'series_navigation', 'menu_class' => 'nav']);  
      }
      elseif (is_page() || is_single() || is_front_page() || is_home())
      {
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
      }
      elseif (has_nav_menu('series_navigation'))
      {
        wp_nav_menu(['theme_location' => 'series_navigation', 'menu_class' => 'nav']);
      }
      ?>
    </nav>
  <!-- </div> -->
</header>
