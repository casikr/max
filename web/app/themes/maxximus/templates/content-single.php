<?php while (have_posts()) : the_post(); ?>
  <article id="singlePost" <?php post_class(); ?>>
    <figure class="img-fixed">
      <div class="container">
        <?php the_post_thumbnail('blogImage', array("class" => "img-fluid img-full")); ?>


      </div>
    </figure>
      <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>

    </header>
    <hr>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
  </article>
<?php endwhile; ?>
