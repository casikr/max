<?php
$qo = get_queried_object();
$term = get_term($qo->term_id, 'series');
?>
<?php if (!have_posts()) : ?>
<article class="catDesc">
  <div class="container-fluid">
  <div class="catDescription">

    <p>
      <?php
      echo $term->description;
      //var_dump($term);
      ?>
    </p>
    </div>
  </div>
</article>


<?php endif; ?>
<?php if($term->parent != 0) { ?>
<?php $counter = 0; ?>
<?php while (have_posts()) : the_post(); ?>
    <?php $counter++; ?>
<?php endwhile; ?>
<?php $cntr = 0; ?>
<?php while (have_posts()) : the_post(); ?>


    <?php if (has_post_thumbnail()) {
    $thumb = get_the_post_thumbnail_url();
    $width = "50%";
    $height = "50%";

    $cntr ++;


    switch($counter)
    {
        case 1:
        $width = "100%";
        $height = "100%";
        break;
        case 2:
        $width = "50%";
        $height = "100%";
        break;
        case 3:
        $width = "33.3332%";
        $height = "100%";
        break;
        case 4:
        $width = "50%";
        $height = "50%";
        break;
        case 5:
        if($cntr < 3) {
            $width = "50%";
        } else {
            $width = "33.3332%";
        }
        $height = "50%";
        break;
        case 6:
        $width = "50%";
        $height = "33.3332%";
        break;
        default:
        $width = "50%";
        $height = "50%";


    }
    ?><!--
    --><a class="prodWrap" style="background-image: url(<?php echo esc_url($thumb); ?>);width: <?php echo $width; ?>; height: <?php echo $height; ?>;" href="<?php esc_url(the_permalink()); ?>"><div class="prod">
    <?php the_title('<h2>','</h2>'); ?>

    </div></a><!--
    --><?php } ?>
    <?php endwhile; ?>

    <?php $counter = 0; ?>
    <?php } else  { ?>
        <div class="container-fluid cats">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <?php $series_img = get_field('series_img', 'series_' . $term->term_id);
              //var_dump($term);
              ?>
              <figure>
                <?php if($series_img) { ?>
                  <img class="img-fluid img-full" src="<?php echo esc_url($series_img['url']); ?>" alt="<?php $series_img['alt']; ?>">
                <?php } ?>
              </figure>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="catDescription">
                <h2><?php _e("Products", "max"); ?></h2>
                <hr>
                <h3><?php echo $term->name; ?></h3>
                <hr>
                <?php
                $child = get_term_children( $term->term_id, 'series' );
                var_dump($child);
                if($child) {
                  $out = '';
                  $out .= '<ul>';


                  foreach($child as $c) {
                    $les = get_term_link(intval($c), 'series');
                    $t = get_term($c, 'series');
                    $out .= '<li><a href="' . esc_url($les). '">' . $t->name . '</a></li>';

                  }

                  $out .= '</ul>';
                  echo $out;
                }



                ?>
                <p>
                  <?php
                  echo $term->description;

                  ?>
                </p>
                </div>
            </div>
          </div>

        </div>
      <?php } ?>
