/**
 * Double tab to go
 * @type {Object}
 */
(function ($) {
    'use strict';
    var options = {},
        currentTap = $(),
        preventClick = false,
        tapEvent = function (event) {
            var $target = $(event.target).closest('li');
            if (!$target.hasClass(options.selectorClass)) {
                preventClick = false;
                return;
            }
            
            if ($target.get(0) === currentTap.get(0)) {
                preventClick = false;
                return;
            }
            preventClick = true;
            currentTap = $target;
            event.stopPropagation();
        };

    $.widget('dcd.doubleTapToGo', {
        options: {
            automatic: true,
            selectorClass: 'doubletap',
            selectorChain: 'li:has(ul)'
        },
        _create: function () {
            var self = this;

            if (window.ontouchstart === undefined && !window.navigator.msMaxTouchPoints && !window.navigator.userAgent.toLowerCase().match(/windows phone os 7/i)) {
                return;
            }

            options = this.options;

            if (window.navigator.msPointerEnabled) {
                this.element.get(0).addEventListener('MSPointerDown', tapEvent, false);
            } else if (window.navigator.pointerEnabled) {
                this.element.get(0).addEventListener('pointerdown', tapEvent, false);
            }

            this.element.on('click', '.' + this.options.selectorClass, function (event) {
                return self._click(event);
            }).on('touchstart', '.' + this.options.selectorClass, function (event) {
                return self._tap(event);
            });

            this._addSelectors();
        },

        _init: function () {
            currentTap = $();
            preventClick = false;
        },

        _addSelectors: function () {
            if (this.options.automatic !== true) {
                return;
            }

            this.element.find(this.options.selectorChain).addClass(this.options.selectorClass);
        },

        _click: function (event) {
            if (preventClick) {
                event.preventDefault();
            } else {
                currentTap = $();
            }
        },

        _tap: tapEvent,

        _destroy: function () {
            this.element.off();

            if (window.navigator.msPointerEnabled) {
                this.element.get(0).removeEventListener('MSPointerDown', tapEvent);
            }

            if (window.navigator.pointerEnabled) {
                this.element.get(0).removeEventListener('pointerdown', tapEvent);
            }
        }
    });
}(jQuery));
/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {
  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
        // $('.seriesContent').masonry({
        //   itemSelector: '.prod',
        //   columnWidth: 0
        // });
        //     var current = window.location.href;
        //     console.log(current);
        //    $('.nav li a').each(function(){
        //        var $this = $(this);
        //        // if the current path is like this link, make it active
        //        if($this.attr('href').indexOf(current) !== -1 && window.location.pathname !== '/'){
        //            $this.addClass('red');
        //        }
        //        if($this.attr('href').indexOf('produkt') !== -1 && window.location.pathname !== '/'){
        //            $this.addClass('red');
        //        }
           //
        //    });

        $("#ham").click(function(){
            $('.nav').slideToggle();
        });

        console.log($(window).width());

        if($(window).width() < 768) {
            $('.menu-item-has-children > a').click(function(e) {
                console.log('ok');
                e.preventDefault();
            });
        }


    //     $(function () {
    //        $('.nav').doubleTapToGo({
    //            'automatic': false,
    //            'selectorClass': 'mene-item-has-children',
    //            'selectorChain': 'li:has(ul)'
    //        });
    //    });

        $('.menu-item-21 > a').click(function(e) {
            e.preventDefault();
            var link = $(this).attr('href');
            $('.nav').fadeOut();
            $('.nav-primary').load(link + ' .menu-series-container', function() {
                $('.wrap').fadeOut();
                $('.nav').fadeIn();
            });
        });
        $('.menu-item-194 > a').click(function(e) {
            e.preventDefault();
            var link = $(this).attr('href');
            $('.nav').fadeOut();
            $('.nav-primary').load(link + ' .menu-series-en-container', function() {
                $('.wrap').fadeOut();
                $('.nav').fadeIn();
            });
        });
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
