<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php', // Theme customizer
  'post-types/product.php', // Theme customizer
  'taxonomies/series.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);
//Disable admin bar
//show_admin_bar(false);
//
/**
 * [pagination description]
 * @method pagination
 * @param  string     $prev [description]
 * @param  string     $next [description]
 * @return [type]           [description]
 */
function pagination($prev = '<', $next = '>') {
    global $wp_query, $wp_rewrite;
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
    $pagination = array(
        'base' => @add_query_arg('paged','%#%'),
        'format' => '',
        'total' => $wp_query->max_num_pages,
        'current' => $current,
        'prev_text' => __($prev),
        'next_text' => __($next),
        'type' => 'plain'
);
    if( $wp_rewrite->using_permalinks() )
        $pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );

    if( !empty($wp_query->query_vars['s']) )
        $pagination['add_args'] = array( 's' => get_query_var( 's' ) );

    echo paginate_links( $pagination );
};
/**
 * Get languages for proper oprions translate
 */
if (function_exists('icl_get_languages')) {
    add_filter('acf/settings/default_language', 'my_acf_settings_default_language');
    function my_acf_settings_default_language( $language )
    {
        return 'pl';
    }
    if(ICL_LANGUAGE_CODE == "en")
    {
        add_filter('acf/settings/current_language', 'my_acf_settings_current_language');
        function my_acf_settings_current_language( $language ) {
            return 'en';
        }
    } else
    {
        add_filter('acf/settings/current_language', 'my_acf_settings_current_language');
        function my_acf_settings_current_language( $language ) {
            return 'pl';
        }
    }
}
/**
 * [my_post_count_queries description]
 * @method my_post_count_queries
 * @param  [type]                $query [description]
 * @return [type]                       [description]
 */
function my_post_count_queries( $query ) {
  if (!is_admin() && $query->is_main_query()){
    if(is_home()){
       $query->set('posts_per_page', 1);
    }
  }
}
add_action( 'pre_get_posts', 'my_post_count_queries' );

/**
 * [icl_post_languages description]
 * @method icl_post_languages
 * @return [type]             [description]
 */
function icl_post_languages(){
  $languages = icl_get_languages('skip_missing=1');
  if(1 < count($languages)){
    foreach($languages as $l){
        if($l['active'] == 0) {
              $langs[] = '<a href="'.$l['url'].'"><img src="' . esc_url($l['country_flag_url']). '" alt="' . $l['native_name']. '" /></a>';
          }
    }
    echo join(', ', $langs);
  }
}
