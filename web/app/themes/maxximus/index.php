<?php //get_template_part('templates/page', 'header'); ?>
<?php
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
query_posts(array('post_type' => 'post', 'posts_per_page' => 4, 'paged' => $paged)); ?>
<?php if (!have_posts()) : ?>
  <!-- <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div> -->
  <?php //get_search_form(); ?>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padNav">

    <nav class="pagination">
        <?php pagination(); ?>
    </nav>
    <?php the_posts_navigation(array(
        'prev_text' => __( 'Pierwsza strona' ),
        'next_text' => __( 'Ostatnia strona' )
    )); ?>
</div>
<?php wp_reset_query(); ?>
