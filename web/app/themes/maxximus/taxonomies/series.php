<?php

function series_init() {
	register_taxonomy( 'series', array( 'product' ), array(
		'hierarchical'      => true,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array("slug" => "seria"),
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Series', 'max' ),
			'singular_name'              => _x( 'Series', 'taxonomy general name', 'max' ),
			'search_items'               => __( 'Search series', 'max' ),
			'popular_items'              => __( 'Popular series', 'max' ),
			'all_items'                  => __( 'All series', 'max' ),
			'parent_item'                => __( 'Parent series', 'max' ),
			'parent_item_colon'          => __( 'Parent series:', 'max' ),
			'edit_item'                  => __( 'Edit series', 'max' ),
			'update_item'                => __( 'Update series', 'max' ),
			'add_new_item'               => __( 'New series', 'max' ),
			'new_item_name'              => __( 'New series', 'max' ),
			'separate_items_with_commas' => __( 'Separate series with commas', 'max' ),
			'add_or_remove_items'        => __( 'Add or remove series', 'max' ),
			'choose_from_most_used'      => __( 'Choose from the most used series', 'max' ),
			'not_found'                  => __( 'No series found.', 'max' ),
			'menu_name'                  => __( 'Series', 'max' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'series',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'series_init' );
